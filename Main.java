import java.util.Arrays;

class Main {

    public static void main(String[] args) {

        // variabel untuk soal 3
        String kata = "souvenir loud four lost last pill cool cold love feel";
        int angka = 4;

        // variabel untuk soal 1
        int[] nums = { 3, 1, 4, 2 };

        // variabel untuk soal 2
        int[] numero = { 1, 2, 3, 4 };
        int x = 4;
        int index = 0;

        try {
            System.out.println(Arrays.toString(soal1(nums, index)));
            System.out.println(Arrays.toString(soal2(numero, x, index)));
            System.out.println(Arrays.toString(soal3(angka, kata)));
        } catch (Exception e) {
            System.out.println("something wnt wrong");
        }

    }

    public static String[] soal3(int angka, String kata) {
        String[] arrKata = kata.split(" ");
        String[] hasil = {};

        for (String a : arrKata) {
            if (a.length() == angka) {
                hasil = addX(hasil.length, hasil, a);
            }
        }
        return hasil;
    }

    public static int[] soal1(int[] nums, int index) {
        int[] hasil = {};
        int kurangi = nums[index];

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] - kurangi > 0) {
                hasil = addN(hasil.length, hasil, nums[i]);
            }
        }
        return hasil;
    }

    public static int[] soal2(int[] numero, int x, int index) {
        int[] hasil = {};
        int indexArr = index;
        int bagi = numero[indexArr];

        for (int a = 0; a < numero.length; a++) {
            if (numero[a] / bagi != x) {
                hasil = addN(hasil.length, hasil, numero[a]);
            }
        }
        return hasil;
    }

    public static int[] addN(int n, int arr[], int x) {
        int i;
        int newArr[] = new int[n + 1];

        for (i = 0; i < n; i++)
            newArr[i] = arr[i];

        newArr[n] = x;

        return newArr;
    }

    public static String[] addX(int n, String arr[], String x) {
        int i;
        String newArr[] = new String[n + 1];

        for (i = 0; i < n; i++) {
            newArr[i] = arr[i];
        }
        newArr[n] = x;

        return newArr;
    }
}
